/*
 * main.c
 *
 *  Created on: 20 Nov 2021
 *   Author: Anie Benita
 *
 */


#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <sclog4c/sclog4c.h>

#include <string.h>
#include "objects.h"
#include "header.h"

#define ADDRESS     "tcp://test.mosquitto.org:1883"
#define CLIENTID    "zoho-mqtt-c123456"
#define TOPIC    	"MyTopic-123456"

int main(int argc, char *argv[]) {

	sclog4c_level = SL4C_ERROR;

	// generate Random data
	struct random_ts_ob rd =generate_random_date();

	logm(SL4C_DEBUG, "generated Random TimeStamp value: %d", rd.ts_value);
	logm(SL4C_DEBUG, "generated Random Date vale: %s", rd.date_value);

	// convert to as payload
	char *payloadObj=malloc(sizeof(char*));
	payloadObj=getJsonObj(rd);
	logm(SL4C_DEBUG, "generated Random payload JSON data: %s", payloadObj);

	send_message_to_server(ADDRESS, CLIENTID, TOPIC, payloadObj);
}


