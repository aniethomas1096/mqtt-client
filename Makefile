IDIR =/usr/local/include
CC=gcc
CFLAGS=-I$(IDIR)
TARGET= mqtt-client #target file name


${TARGET}: 
	gcc -L$(IDIR) main.c MQTTClient_publish.c random_util.c json_print.c -o ${TARGET} -ljson-c -lsclog4c -lpaho-mqtt3c

clean:
	rm -rf ${TARGET}
