/*
 * header.h
 *
 *  Created on: 22 Nov 2021
 *  Author: Anie Benita
 */

#ifndef HEADER_H_
#define HEADER_H_
#include <sys/types.h>
#include <json-c/json.h>
#include "objects.h"
#include <sclog4c/sclog4c.h>


int generate_random_value(int,int );
int send_message_to_server(char *address, char *client_id, char *topic,char * payloadObj);
int convert_timestamp(int, int, int, int, int, int);
struct random_ts_ob generate_random_date();
char* getJsonObj(struct random_ts_ob rd);

#endif /* HEADER_H_ */
