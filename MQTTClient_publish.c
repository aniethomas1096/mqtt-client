#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MQTTClient.h"



#define QOS         1
#define TIMEOUT     10000L

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <json-c/json.h>

#include <string.h>
#include "objects.h"
#include "header.h"

int send_message_to_server(char *address, char *client_id, char *topic,char *payloadObj)
{
    
	char* PAYLOAD=payloadObj;
    
	MQTTClient client;
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	MQTTClient_message pubmsg = MQTTClient_message_initializer;
	MQTTClient_deliveryToken token;
	int rc;

	if ((rc = MQTTClient_create(&client, address, client_id,
	MQTTCLIENT_PERSISTENCE_NONE, NULL)) != MQTTCLIENT_SUCCESS)
	{
	 logm(SL4C_ERROR, "Failed to create client, return code %d", rc);
	 exit(EXIT_FAILURE);
	}

	conn_opts.keepAliveInterval = 20;
	conn_opts.cleansession = 1;
	if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
	{
	  logm(SL4C_ERROR, "Failed to create client, return code %d", rc);
	  exit(EXIT_FAILURE);
	}

	pubmsg.payload = PAYLOAD;
	pubmsg.payloadlen = (int)strlen(PAYLOAD);
	pubmsg.qos = QOS;
	pubmsg.retained = 0;
	if ((rc = MQTTClient_publishMessage(client, topic, &pubmsg, &token)) != MQTTCLIENT_SUCCESS)
	{
		logm(SL4C_ERROR, "Failed to publish message, return code %d", rc);
		exit(EXIT_FAILURE);
	}

//	printf("Waiting for up to %d seconds for publication of %s\n"
//	    "on topic %s for client with ClientID: %s\n",
//	    (int)(TIMEOUT/1000), PAYLOAD, topic, client_id);
	logm(SL4C_INFO, "Waiting for up to %d seconds for publication of %s on topic %s for client with ClientID: %s",(int)(TIMEOUT/1000), PAYLOAD, topic, client_id);
	rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
	printf("\n\n######    SUCCESS 	 ############\n");
	printf("Message with delivery token %d delivered\n", token);

	if ((rc = MQTTClient_disconnect(client, 10000)) != MQTTCLIENT_SUCCESS)
	// printf("Failed to disconnect, return code %d\n", rc);
	logm(SL4C_ERROR, "Failed to disconnect, return code %d", rc);
	MQTTClient_destroy(&client);
	return rc;
}
