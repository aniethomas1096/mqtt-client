/*
 * random.h
 *
 *
 *  Created on: 21 Nov 2021
 *  Author: Anie Benita
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "objects.h"
#include "header.h"

/**
 * This method will create a random number with in the given range.
 * @param start_range
 * @param end_range
 * @return
 */

int  generate_random_value(int start_range,int end_range) {
	int a;
	srand ( time(NULL) );
    a = rand();
	return  (a % (end_range - start_range + 1)) + start_range;
}

/**
 * This method will take the date as parameter and converts it to TimeStamp
 * @param yrs
 * @param mnth
 * @param dte
 * @param hour
 * @param minute
 * @param secs
 * @return
 */
int convert_timestamp(int yrs, int mnth, int dte, int hour, int minute, int secs) {
	    struct tm t;
		time_t time_stamp;
		t.tm_year = yrs - 1900;
		t.tm_mon = mnth -1;
		t.tm_mday = dte;
		t.tm_hour = hour;
		t.tm_min = minute;
		t.tm_sec = secs;
		t.tm_isdst = -1;
		time_stamp = mktime(&t);
		return time_stamp;
}

/**
 * This method will take the date as parameter and converts it to object that
 * contains human readable date and its TimeStamp
 * @return
 */
 struct random_ts_ob generate_random_date () {

	 int year=generate_random_value(1990, 2030);
	 int month=generate_random_value(1, 12);

	 // if month is feb and leap yr - no of date should be calculated as below
	 int max_days_in_month=(month % 2 == 0)?31:30;
	 if(month == 2){
		 max_days_in_month=(year%4==0)?29:28;
	 }

	 int day_in_month=generate_random_value(1, max_days_in_month);

	 int hrs=generate_random_value(0, 23);
	 int min=generate_random_value(0, 59);
	 int sec=generate_random_value(0, 59);

	 // char *date_val;

	 struct random_ts_ob rd;

	 sprintf(rd.date_value,"%d-%02d-%02d %02d:%02d:%02d", year, month, day_in_month, hrs, min, sec);

	 logm(SL4C_DEBUG, "Date Value %s", rd.date_value);
	 int timestap_value=convert_timestamp(year, month, day_in_month, hrs, min, sec);
	 logm(SL4C_DEBUG, "TimeStamp Value against the Date Value %d", timestap_value);


	 rd.ts_value=timestap_value;

	 return rd;

}




