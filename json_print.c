/*
 * Various outputs for flag of json_object_to_json_string_ext().
 *
 * clang -Wall -I/usr/include/json-c/ -o json_print json_print.c -ljson-c
 */

#include <stdio.h>
#include <json-c/json.h>
#include <string.h>
#include "objects.h"
#include "header.h"
 

char* getJsonObj(struct random_ts_ob rd){

	logm(SL4C_DEBUG, "Received date value %s", rd.date_value);
	logm(SL4C_DEBUG, "Received TimeStamp value %d", rd.ts_value);
	json_object *root = json_object_new_object();
	
	char *strValue=malloc(sizeof(char*));
	char *dateValue=malloc(sizeof(char*));
	
	sprintf(strValue,"%d",rd.ts_value);
	sprintf(dateValue,"%s",rd.date_value);
	

	json_object_object_add(root, "timestampValue", json_object_new_string(strValue));
	json_object_object_add(root, "dateValue", json_object_new_string(dateValue));
    	

	logm(SL4C_DEBUG, "JSON Object created as :: %s",json_object_to_json_string_ext(root,JSON_C_TO_STRING_PLAIN));
	char *jsonVal=malloc(sizeof(char*));
	jsonVal = (char*) json_object_to_json_string_ext(root,JSON_C_TO_STRING_PLAIN);

	printf("\n\n Json Payload :: %s ", jsonVal);
   	// json_object_put(root);
   	
   	return jsonVal;
}
